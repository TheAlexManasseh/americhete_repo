/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>

char buffer[100];

#define NONE 0

#define DEBUG_MODE NONE
#define SERIAL_DEBUG 1

#define RUN_MODE I2C_READ_OP
#define I2C_READ_OP 1u
#define I2C_WRITE_OP 2u

#define ACC_ADDR (uint8_t)(0x0Fu) //WHO_AM_I register address

#define CTRL_REG1 0x1B
#define CTRL_REG1_VAL 0b01111000
#define CTRL_REG1_PC1 0b11111000
#define ACC_WRITE_ADDR CTRL_REG1

#define ADDR_WHO_AM_I (uint8_t)0x0Fu

#define LOOP_DELAY 50 //Delay at the end of each loop, in ms

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    uint8_t data0=100, data1=200;
    uint8_t read_byte[100];
    volatile uint32_t result_code;
    
    debug_uart_Start();
    I2C_SensorNet_Start();
//    acc_int_Start();
        
#if (DEBUG_MODE == SERIAL_DEBUG)
    sprintf(buffer, "$%u %u;", data0, data1);
    debug_uart_UartPutString(buffer);
    
    if( data0 < 255)
    {
        data0++;
    }
    else
    {
        data0 = 0;
    }
    
    if( data1 < 255)
    {
        data1++;
    }
    else
    {
        data1 = 0;
    }
#elif(RUN_MODE == I2C_READ_OP)
    //Read Operation: S -> SAD+W -> /ACK -> RA -> /ACK -> Sr -> SAD+R -> /ACK -> /DATA -> NACK -> P
    
    // S = Start, SAD = Slave Address, W = Write bit, ACK = acknowledge, RA = Register Address, DATA = data, P = STOP
    
    result_code = I2C_SensorNet_I2CMasterSendStart(ACC_ADDR, I2C_SensorNet_I2C_WRITE_XFER_MODE, 1); //prime a read from accelerometer
    result_code = I2C_SensorNet_I2CMasterWriteByte( ADDR_WHO_AM_I, 1);
    CyDelayUs(10);
    result_code = I2C_SensorNet_I2CMasterSendRestart(ACC_ADDR, I2C_SensorNet_I2C_READ_XFER_MODE, 1); //prime a read from accelerometer
    result_code = I2C_SensorNet_I2CMasterReadByte(I2C_SensorNet_I2C_NAK_DATA, read_byte, 1);
    I2C_SensorNet_I2CMasterSendStop(10);
    
    sprintf(buffer, "\r\nAddress: 0x%X\tRegister Address: 0x%X\tMessage Received: 0x%X", ACC_ADDR, ADDR_WHO_AM_I, read_byte[0]);
    debug_uart_UartPutString(buffer);
        
#elif(RUN_MODE == I2C_WRITE_OP)

    //Write Operation: S -> SAD+W -> /ACK -> RA -> /ACK -> DATA -> /ACK -> P
    
    // S = Start, SAD = Slave Address, W = Write bit, R = Read bit, ACK = acknowledge, RA = Register Address, 
                    //DATA = data, NACK = No ack, P = STOP
    
    result_code = I2C_SensorNet_I2CMasterSendStart(ACC_ADDR, I2C_SensorNet_I2C_WRITE_XFER_MODE, 1); //prime a read from accelerometer
    result_code = I2C_SensorNet_I2CMasterWriteByte( CTRL_REG1, 1);
    result_code = I2C_SensorNet_I2CMasterWriteByte( CTRL_REG1_VAL, 1);
    CyDelayUs(10);
    I2C_SensorNet_I2CMasterSendStop(10);
    
    sprintf(buffer, "\r\nAddress: 0x%X\tRegister Address: 0x%X\tMessage Received: 0x%X", ACC_ADDR, CTRL_REG1, CTRL_REG1_PC1);
    debug_uart_UartPutString(buffer);
        
#endif

    for(;;)
    {
        /* Place your application code here. */
        CyDelay(LOOP_DELAY);
    }
}

/* [] END OF FILE */
